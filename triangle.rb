# Triangle Project Code.

# Triangle analyzes the lengths of the sides of a triangle
# (represented by a, b and c) and returns the type of triangle.
#
# It returns:
#   :equilateral  if all sides are equal
#   :isosceles    if exactly 2 sides are equal
#   :scalene      if no sides are equal
#
# The tests for this method can be found in
#   about_triangle_project.rb
# and
#   about_triangle_project_2.rb
#
def triangle(a, b, c)
  # WRITE THIS CODE
  args = [a, b, c]
  args.sort!

  args.each { |x| if x <= 0 then raise TriangleError, "sides must be >0" end }
  if args[0] + args[1] <= args[2] then raise TriangleError, "2 sides too short" end
  if args.uniq.length == 3 then return :scalene
  elsif args.uniq.length == 2 then return :isosceles
  elsif args.uniq.length == 1 then return :equilateral
  end
end


# Error class used in part 2.  No need to change this code.
class TriangleError < StandardError
end
